# ---------------------------------------------------------
# ---------- Zillow House Price Data ----------------------
# ---------------------------------------------------------

using DataFrames, Statistics, CSV

url = "http://files.zillowstatic.com/research/public/County/County_Zhvi_AllHomes.csv"

# CSV.read(url=url)
# 
# download(url)

df_zillow = CSV.read("data/County_Zhvi_AllHomes.csv")  

select!(df_zillow, Not([:Metro, :RegionName, :State, :RegionID, :SizeRank]))
rename!(df_zillow, :StateCodeFIPS => :state, :MunicipalCodeFIPS => :county)

df_melted = stack(df_zillow, Not([:state, :county]), value_name=:hpi)

df_yyyymm = map(df_melted.variable) do var
  yyyymm = string.(split(string(var), "-"))
  yyyymm_int = parse.(Int, yyyymm)
  NamedTuple{(:year, :month)}(Tuple(yyyymm_int))
end |> DataFrame

df_melted = [df_melted df_yyyymm]

filter!(row -> row[:year] <= 2016, df_melted)
select!(df_melted, Not(:variable))

df_hpi_ann = by(df_melted, [:state, :county, :year], :hpi => mean)

Feather.write("data-processed/hpi-county.feather", df_hpi_ann)

# ---------------------------------------------------------
# ---------- HMDA Mortgage Data ---------------------------
# ---------------------------------------------------------

using RCall, DataFrames

R"""load("data-processed/hmda_big.RData")"""
R"""load("data-processed/hmda_inc_big.RData")"""
@rget hmda_big
@rget hmda_inc_big

map([hmda_big, hmda_inc_big]) do hmda
  filter!(row -> !ismissing(row.county), hmda)

  # replace O (oh) by 0 (zero)
  hmda[!,:county] = replace.(hmda.county, Ref('O' => '0'))
  # remove all rows with invalid county identifier
  hmda[!,:state] = Int.(hmda.state)
  hmda[!,:county] = tryparse.(Int, string.(hmda.county))
  filter!(row -> !isnothing(row.county), hmda)
  hmda[!,:county] = Int.(hmda.county)

  # use only owner-occupied houses, remove multi-family houses (TODO)
  filter!(row -> !ismissing(row[:purpose_loan]) && row[:purpose_loan] ∈ ["improvement", "refinancing", "purchase"], hmda)
  filter!(row -> !ismissing(row[:owner_occupied]) && row[:owner_occupied], hmda)
end

# # remove county-year pairs with too few observations
#filter!(row -> row[:count] > 30, hmda_big)
#filter!(row -> row[:count] > 20, hmda_inc_big)
# # DO THIS FOR EACH REGRESSION SEPARATELY!

hmda_big
hmda_inc_big

using Feather
Feather.write("data-processed/hmda_big.feather", hmda_big)
Feather.write("data-processed/hmda_inc_big.feather", hmda_inc_big)

# ---------------------------------------------------------
# ---------- Inequality data ------------------------------
# ---------------------------------------------------------

using StatFiles, DataFrames, ShiftedArrays, DataFramesMeta
using Lazy

cps = CSV.read("data-processed/cps-state-panel.csv") |> DataFrame

#cps0[!,:state] = CategoricalArray(lpad.(Int.(cps0.state),  2, '0'))

cps[!,:p90p50] = cps.p90 ./ cps.p50

sort!(cps, :year)

cps2 = by(cps, :state) do df
  @> begin
    df
    @transform(p90p50_Δ1 = :p90p50 ./ lag(:p90p50, 1) .- 1)
    @transform(p90p50_Δ2 = :p90p50 ./ lag(:p90p50, 2) .- 1)
    @transform(p90p50_Δ3 = :p90p50 ./ lag(:p90p50, 3) .- 1)
    @transform(p90p50_Δ4 = :p90p50 ./ lag(:p90p50, 4) .- 1)
    @transform(p90p50_Δ5 = :p90p50 ./ lag(:p90p50, 5) .- 1)
    @transform(p90p50_Δ6 = :p90p50 ./ lag(:p90p50, 6) .- 1)
    @transform(p90p50_Δ7 = :p90p50 ./ lag(:p90p50, 7) .- 1)
    @transform(Δp90p50_L1 = lag(:p90p50_Δ1, 1))
    @transform(Δp90p50_L2 = lag(:p90p50_Δ1, 2))
    @transform(Δp90p50_L3 = lag(:p90p50_Δ1, 3))
    @transform(Δp90p50_L4 = lag(:p90p50_Δ1, 4))
    @transform(Δp90p50_L5 = lag(:p90p50_Δ1, 5))
    @transform(Δp90p50_L6 = lag(:p90p50_Δ1, 6))
    @transform(Δp90p50_L7 = lag(:p90p50_Δ1, 7))
  end
end

Feather.write("data-processed/cps-lags.feather", cps2)

# ---------------------------------------------------------
# ---------- Merge ----------------------------------------
# ---------------------------------------------------------

hmda_hpi = join(hmda_big, df_hpi_ann, kind=:left, on=[:state, :county, :year])
hmda_inc_hpi = join(hmda_inc_big, df_hpi_ann, kind=:left, on=[:state, :county, :year])

hmda_hpi_ineq = join(hmda_hpi, cps2, kind=:left, on=[:state, :year])
hmda_inc_hpi_ineq = join(hmda_inc_hpi, cps2, kind=:left, on=[:state, :year])

CSV.write("data-processed/hmda_hpi_ineq.csv", hmda_hpi_ineq)
CSV.write("data-processed/hmda_inc_hpi_ineq.csv", hmda_inc_hpi_ineq)


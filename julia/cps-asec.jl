# ---------------------------------------------------------
# ---------- CPS ASEC Income Data -------------------------
# ---------------------------------------------------------

using StatFiles, CSV, DataFrames
using StatsBase, Statistics

isdir("data-processed") || mkdir("data-processed")

using Tables, Query
# read data downloaded from iPums
df = load("data-unzipped/cps_00001.dta") |> 
  # keep only ASEC entries
  Query.@filter(_.asecflag != NA && _.asecflag == 1) |>
  # keep only relevant variables
  Query.@select(:asecflag, :hhincome, :asecwth, :year, :statefip, :cpi99) |>
  # kick out duplicates (there is one line per person, not per household)
  unique |>
  DataFrame

# check that there are no missings in the relevent variables
@assert mean(ismissing.(df.asecwth))  == 0
@assert mean(ismissing.(df.hhincome)) == 0
df[!,:asecwth] = float.(df.asecwth)

rename!(df, :statefip => :state)

# compute 
df_q = by(df, [:year, :state, :cpi99]) do sdf
  inc = sdf[!, :hhincome]
  wgt = sdf[!, :asecwth]
  m = mean(inc, Weights(wgt))
  p = 10:10:90
  q = quantile(inc, Weights(wgt), p./100)
  keys = Tuple(Symbol.(["p$pp" for pp in p]))
  (NamedTuple{keys}(Tuple(q))..., m=m)
end

CSV.write("data-processed/cps-state-panel.csv", df_q)

cps = df_q

#=
# compare with Betrand Morse
cps0 = load("data/BertrandMorseTrickleCEXData.dta") |> DataFrame
cps0[!,:state] = CategoricalArray(lpad.(Int.(cps0.state),  2, '0'))
cps0[!,:year] = Int.(cps0.year)

select!(cps0, [:state, :year, :def99], r"^p") |> unique!

using Test
let
  count05 = 0
  count075 = 0
  count10 = 0
  count15 = 0
  count100 = 0

  for row0 in eachrow(unique!(filter!(row -> row.year > 1990, cps0)))
    yr = row0.year
    st = row0.state

    out1 = filter(row -> row.year == yr && row.state==parse(Int,st), cps)
    for p in Symbol.(["p$i" for i in 10:10:90])
      Δ = row0[p] .* row0[:def99] / out1[1,p] - 1
      if abs(Δ) < 0.05
        count05 += 1
      elseif abs(Δ) < 0.075
        count075 += 1  
      elseif abs(Δ) < 0.1
        count10 += 1
      elseif abs(Δ) < 0.15
        count15 += 1
      else
        count100 += 1
      end
    end
  end
  @show count05, count10, count15, count100
end
# (2213, 586, 533, 193)
=#

# TODO compute state panel from publicly available data
# TODO compare to state panel used by Bertrand morse

# http://data.nber.org/data/current-population-survey-data.html
# http://data.nber.org/data/cps_progs.html

# http://asdfree.com/current-population-survey-annual-social-and-economic-supplement-cpsasec.html
# http://data.nber.org/data/current-population-survey-data.html

# ---------------------------------------------------------
# -- Download MARCH CPS Data 2008--2016 -------------------
# ---------------------------------------------------------

# cps_march_url_pre = "http://thedataweb.rm.census.gov/pub/cps/march/"
# 
# cps_march_url_post(yyyy) = "asec$(yyyy)_pubuse.dat.gz"
# 
# download("asec2007_pubuse_tax2.dat.gz", "data-raw/asec2007.dat.gz")
# 
# yr = 2008
# map(2009:2016) do yr
#   download(cps_march_url_pre * cps_march_url_post(yr), "data-raw/cps_march_$yr.dat.gz")
# end
# 
# http://thedataweb.rm.census.gov/pub/cps/march/08ASEC2018_Data_Dict_Full.txt
# http://thedataweb.rm.census.gov/pub/cps/march/asec2013early_pubuse.dd.txt
# 
# map(7:7) do yr
#   yy = lpad(yr, 2, '0')
#   download("http://data.nber.org/data/progs/cps/cpsmar$yy.sas", "data-raw/cpsmar$yy.sas")
# end
# 
# http://data.nber.org/data/progs/cps/cpsmar08.sas
# http://data.nber.org/data/progs/cps/cpsmar07.sas
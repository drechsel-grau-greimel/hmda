using ProgressMeter

zip_files = [
  "HMS.F1997.LARS",
  "HMS.F1998.LARS",
  "HMS.F1999.LARS",
  "HMS.F2000.LARS",
  "HMS.F2001.LARS",
  "HMS.F2002.LARS",
  "HMS.F2003.LARS",
  "f2004lar.public.dat",
  "LARS.FINAL.2005.DAT",
  "LARS.FINAL.2006.DAT",
  "lars.final.2007.dat",
  "lars.final.2008.dat",
  "2009_Final_PUBLIC_LAR.dat",
  "Lars.final.2010.dat",
  "Lars.final.2011.dat",
  "Lars.final.2012.dat",
  "Lars.final.2013.dat",
  "FLAR_2014",
  "2015HMDALAR - National",
  "2016HMDALAR - National"]

progress_map(zip_files) do zip_file
  run(`unzip data-raw/$zip_file.zip -d data-unzipped/`)
  rm("data-raw/$zip_file.zip")
  yr = match(r"(\d{4})", zip_file)[1]
  file = filter(x -> occursin(yr, x), readdir("data-unzipped/"))[1]
  # rename file using common naming scheme
  mv("data-unzipped/$file", "data-unzipped/hmda-flar-$yr.fwf")
end

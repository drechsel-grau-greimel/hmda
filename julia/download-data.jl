isdir("data-raw") || mkdir("data-raw")
using ProgressMeter

# ---------------------------------------------------------
# -- Download HMDA 1997--2014 from the National Archives --
# ---------------------------------------------------------

archives_hmda_url_pre = "https://catalog.archives.gov/catalogmedia/lz/electronic-records/rg-082/hmda/"

archives_hmda_url_post = [
  "HMS.F1997.LARS.zip",
  "HMS.F1998.LARS.zip",
  "HMS.F1999.LARS.zip",
  "HMS.F2000.LARS.zip",
  "HMS.F2001.LARS.zip",
  "HMS.F2002.LARS.zip",
  "HMS.F2003.LARS.zip",
  "FLAR04/f2004lar.public.dat.zip",
  "FLAR0506/LARS.FINAL.2005.DAT.zip",
  "FLAR0506/LARS.FINAL.2006.DAT.zip",
  "FLAR0708/lars.final.2007.dat.zip",
  "FLAR0708/lars.final.2008.dat.zip",
  "FLAR09/2009_Final_PUBLIC_LAR.dat.zip",
  "FTL10/Lars.final.2010.dat.zip",
  "FTL11/Lars.final.2011.dat.zip",
  "2012/Lars.final.2012.dat.zip",
  "2013/Lars.final.2013.dat.zip",
  "2014/FLAR_2014.zip"
 ]

progress_map(archives_hmda_url_post) do post
  file_name = split(post, "/")[end]
  download(archives_hmda_url_pre * post, "data-raw/"*file_name)  
end

# ---------------------------------------------------------
# -- Download HMDA 2015--2016 from the FFIEC web-site -----
# ---------------------------------------------------------

# Note: Files are available from the FFIEC website after 2006, just change the year in the url

ffiec_hmda_url_pre = "https://www.ffiec.gov/hmdarawdata/LAR/National/"

ffiec_hmda_url_post = [
  "2015HMDALAR%20-%20National.zip",
  "2016HMDALAR%20-%20National.zip"
 ]

progress_map(ffiec_hmda_url_post) do post
  download(ffiec_hmda_url_pre * post, "data-raw/"*post)  
end
